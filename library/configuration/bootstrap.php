<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace Library\Configuration;
use Nette\Configurator;
use Nette\Neon\Exception;
use Tracy\Debugger;

require __DIR__.'/../../vendor/autoload.php';

define("ENV", 0);
define("LOG",__DIR__."/../storage/logger");
define("TMP",__DIR__."/../storage/temporary");
define("MAIL", "pavelka@master.cz");

$configurator = new Configurator;
if (ENV == 0 || ENV == 1) $configurator->setDebugMode(true);
else $configurator->setDebugMode(false);

$configurator->enableTracy(LOG);
$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(TMP);

$configurator->createRobotLoader()
    ->addDirectory(__DIR__."/../sources")
    ->excludeDirectory(__DIR__."/../sources/routines")
    ->register();

$configurator->addParameters(['environment' => ENV]);

if (ENV == 0) $configurator->addParameters(['environmentName' => "Development"]);
else if (ENV == 1) $configurator->addParameters(['environmentName' => "Testing"]);
else if (ENV == 2) $configurator->addParameters(['environmentName' => "Production"]);

$configurator->addConfig(__DIR__.'/config.neon');
if (ENV == 0) $configurator->addConfig(__DIR__.'/development.neon');
else if (ENV == 1) $configurator->addConfig(__DIR__.'/testing.neon');
else if (ENV == 2) $configurator->addConfig(__DIR__.'/production.neon');
if (ENV == 0) $configurator->addConfig(__DIR__.'/developmentSecure.neon');
else throw new Exception("Undefined config for ENV: " . ENV);

if (ENV == 0) Debugger::enable(Debugger::DEVELOPMENT, LOG, MAIL);
else if (ENV == 1) Debugger::enable(Debugger::DEVELOPMENT, LOG, MAIL);
else if (ENV == 2) Debugger::enable(Debugger::PRODUCTION, LOG, MAIL);
if (ENV != 2) Debugger::$logSeverity = E_NOTICE | E_WARNING;
Debugger::$maxLength = 100000;
Debugger::$maxDepth = 100;

return $configurator->createContainer();
