<?php

namespace Library\Sources\Entities;
use Exception;

/**
 * Class ABaseVcloudEntity
 * @package Library\Sources\Entities
 */
abstract class ABaseVCloudEntity {

}

/**
 * Class ABaseVCloudEntityRequiredParameterMissingException
 * @package Library\Sources\Entities
 */
class ABaseVCloudEntityRequiredParameterMissingException extends  Exception {};