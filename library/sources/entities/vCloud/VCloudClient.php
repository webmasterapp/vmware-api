<?php

namespace Library\Sources\Entities;
use VMware_VCloud_API_ReferenceType;

/**
 * Class VCloudClient
 * @package Library\Sources\Entities
 */
class VCloudClient extends ABaseVCloudEntity {

    /** @var string|null */
    public $name;

    /** @var string|null */
    public $password;

    /** @var string|null */
    public $fullName;

    /** @var bool */
    public $enabled;

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $role;

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $organization;

    /** VCloudClient constructor. */
    public function __construct() {

        $this->name = null;
        $this->password = null;
        $this->fullName = null;
        $this->enabled = true;
        $this->role = null;
        $this->organization = null;
    }

    /**
     * Check if possible to use this object
     * @throws ABaseVCloudEntityRequiredParameterMissingException
     */
    public function check() {

        if ($this->organization === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing organization");
        if ($this->role === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing role");
        if ($this->fullName === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing fullName");
        if ($this->password === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing password");
        if ($this->name === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing name");
    }
}