<?php

namespace Library\Sources\Entities;

/**
 * Class VCloudOrganization
 * @package Library\Sources\Entities
 */
class VCloudOrganization extends ABaseVCloudEntity {

    /** @var string|null */
    public $name;

    /** @var string|null */
    public $fullName;

    /** @var string */
    public $description;

    /** @var bool */
    public $canPublishCatalogs;

    /** @var string */
    public $orgLdapMode;

    /** @var int */
    public $storageLeaseSeconds;

    /** @var int */
    public $deploymentLeaseSeconds;

    /** @var bool */
    public $deleteOnStorageLeaseExpiration;

    /** @var bool */
    public $isEnabled;

    /** VCloudOrganization constructor. */
    public function __construct() {

        $this->name = null;
        $this->fullName = null;
        $this->description = "";
        $this->canPublishCatalogs = false;
        $this->orgLdapMode = "NONE";
        $this->storageLeaseSeconds = 0;
        $this->deploymentLeaseSeconds = 0;
        $this->deleteOnStorageLeaseExpiration = false;
        $this->isEnabled = true;
    }

    /**
     * Check if possible to use this object
     * @throws ABaseVCloudEntityRequiredParameterMissingException
     */
    public function check() {

        if ($this->name === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing name");
        if ($this->fullName === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing fullName");
    }
}