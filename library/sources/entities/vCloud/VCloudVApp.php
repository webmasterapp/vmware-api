<?php

namespace Library\Sources\Entities;

use VMware_VCloud_API_ReferenceType;

/**
 * Class VCloudVApp
 * @package Library\Sources\Entities
 */
class VCloudVApp extends ABaseVCloudEntity {

    /** @var string|null */
    public $name;

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $vdc;

    /** VCloudVApp constructor. */
    public function __construct() {

        $this->name = null;
        $this->vdc = null;
    }

    /**
     * Check if possible to use this object
     * @throws ABaseVCloudEntityRequiredParameterMissingException
     */
    public function check() {

        if ($this->name === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing name");
        if ($this->vdc === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing vdc");
    }
}