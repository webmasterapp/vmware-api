<?php

namespace Library\Sources\Entities;
use VMware_VCloud_API_ReferenceType;

/**
 * Class VCloudVDC
 * @package Library\Sources\Entities
 */
class VCloudVDC extends ABaseVCloudEntity {

    /** @var string|null */
    public $name;

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $provider;

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $storageProfile;

    /** @var bool */
    public $usesFastProvisioning;

    /** @var bool */
    public $isThinProvision;

    /** @var bool */
    public $isEnabled;

    /** @var int */
    public $networkQuota;

    /** @var int */
    public $nicQuota;

    /** @var float */
    public $resourceGuaranteedMemory;

    /** @var float */
    public $resourceGuaranteedCpu;

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $organization;

    /** @var int|null - in MHz */
    public $cpuLimit;

    /** @var int - in MHz */
    public $cpuAllocated;

    /** @var int|null - in MB */
    public $ramLimit;

    /** @var int - in MB */
    public $ramAllocated;

    /** @var string */
    public $allocationModel;

    /** @var string */
    public $description;

    /** @var bool */
    public $storageProviderDefault;

    /** @var bool */
    public $storageProfileIsEnabled;

    /** @var int - in MB */
    public $storageProfileLimit;

    /** VCloudVDC constructor. */
    public function __construct() {

        $this->usesFastProvisioning = false;
        $this->isThinProvision = true;
        $this->isEnabled = true;
        $this->provider = null;
        $this->networkQuota = 0;
        $this->nicQuota = 0;
        $this->resourceGuaranteedCpu = 0;
        $this->resourceGuaranteedMemory = 0.2;
        $this->storageProfile = null;
        $this->ramLimit = null;
        $this->cpuLimit = null;
        $this->ramAllocated = 16 * 1024;
        $this->cpuAllocated = 8 * 1024;
        $this->allocationModel = "AllocationPool";
        $this->description = "";
        $this->name = null;
        $this->storageProfileIsEnabled = true;
        $this->storageProfileLimit = 500 * 1024;
        $this->storageProviderDefault = true;
    }

    /**
     * Check if possible to use this object
     * @throws ABaseVCloudEntityRequiredParameterMissingException
     */
    public function check() {

        if ($this->provider === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing provider");
        if ($this->storageProfile === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing storageProfile");
        if ($this->ramLimit === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing ramLimit");
        if ($this->cpuLimit === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing cpuLimit");
        if ($this->name === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing name");
    }
}