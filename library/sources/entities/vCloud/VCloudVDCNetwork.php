<?php

namespace Library\Sources\Entities;

/**
 * Class VCloudVDCNetwork
 * @package Library\Sources\Entities
 */
class VCloudVDCNetwork extends ABaseVCloudEntity {

    /** @var string */
    public $name;

    /** @var string */
    public $fenceMode;

    /** VCloudVDC constructor. */
    public function __construct() {

        $this->name = "internet";
        $this->fenceMode = "bridged";
    }

    /** Check if possible to use this object */
    public function check() {}
}