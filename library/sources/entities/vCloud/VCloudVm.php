<?php

namespace Library\Sources\Entities;

use VMware_VCloud_API_ReferenceType;

/**
 * Class VCloudVm
 * @package Library\Sources\Entities
 */
class VCloudVm extends ABaseVCloudEntity {

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $parentNetwork;

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $vApp;

    /** @var VMware_VCloud_API_ReferenceType|null */
    public $sourceVApp;

    /** @var string */
    public $networkName;

    /** @var string */
    public $fenceMode;

    /** @var bool */
    public $sourceDelete;

    /** @var string */
    public $networkInfo;

    /** @var bool */
    public $isDeployed;

    /** @var string|null */
    public $name;

    /** VCloudVm constructor. */
    public function __construct() {

        $this->parentNetwork = null;
        $this->name = null;
        $this->networkName = "internet";
        $this->fenceMode = "bridged";
        $this->vApp = null;
        $this->sourceVApp = null;
        $this->sourceDelete = false;
        $this->networkInfo = "Virtual network directly bridged to the Internet";
        $this->isDeployed = true;
    }

    /**
     * Check if it is possible to use this object
     * @throws ABaseVCloudEntityRequiredParameterMissingException
     */
    public function check() {

        if ($this->parentNetwork === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing parentNetwork");
        if ($this->vApp === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing vApp");
        if ($this->sourceVApp === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing sourceVApp");
        if ($this->name === null) throw new ABaseVCloudEntityRequiredParameterMissingException("Missing name");
    }
}