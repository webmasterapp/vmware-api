<?php

namespace Library\Sources\Models\VCloud;
use Exception;
use Library\Sources\Services\Parameters;
use Library\Sources\Services\Reporter;
use Nette\Caching\IStorage;
use VMware_VCloud_API_QueryResultOrgRecordType;
use VMware_VCloud_API_ReferenceType;
use VMware_VCloud_SDK_Admin;
use VMware_VCloud_SDK_Extension;
use VMware_VCloud_SDK_Helper;
use VMware_VCloud_SDK_Query;
use VMware_VCloud_SDK_Query_Params;
use VMware_VCloud_SDK_Service;

/**
 * Class ABaseVCloud
 * @package Library\Sources\Models\VCloud
 */
abstract class ABaseVCloud {

    /** @var array @inject */
    protected $containerParameters;

    /** @var Reporter @inject */
    protected $reportService;

    /** @var IStorage @inject */
    private $cacheService;

    /** @var string  */
    private const serviceCacheKey = "vCloudApiService";

    /** @var VMware_VCloud_SDK_Service */
    protected $vCloudService;

    /** @var  VMware_VCloud_SDK_Admin */
    protected $vCloudAdmin;

    /** @var  VMware_VCloud_SDK_Extension */
    protected $vCloudExtension;

    /**
     * ABaseVCloud constructor.
     * @param Parameters $parameters
     * @param IStorage $cacheService
     * @param Reporter $reporter
     * @throws ABaseVCloudServiceAdminCreateException
     * @throws ABaseVCloudServiceExtensionCreateException
     * @throws ABaseVCloudServiceNotFoundInCache
     */
    public function __construct(Parameters $parameters, IStorage $cacheService, Reporter $reporter) {

        $this->containerParameters = $parameters->getParameters();
        $this->cacheService = $cacheService;
        $this->reportService = $reporter;
        $this->setVCloudService();
        $this->setVCloudAdmin();
        $this->setVCloudExtension();
    }

    /**
     * Set vCloud service from cache
     * @throws ABaseVCloudServiceNotFoundInCache
     */
    private function setVCloudService(): void {

        $this->vCloudService = $this->cacheService->read(self::serviceCacheKey);
        if ($this->vCloudService === null)
            throw new ABaseVCloudServiceNotFoundInCache("Service not found with key: ".self::serviceCacheKey);
    }

    /**
     * Set vCloud SDK Admin object
     * @throws ABaseVCloudServiceAdminCreateException
     */
    private function setVCloudAdmin(): void {

        $this->vCloudAdmin = $this->vCloudService->createSDKAdminObj();
        if ($this->vCloudAdmin === null)
            throw new ABaseVCloudServiceAdminCreateException("Can't create SDK Admin object ");
    }

    /**
     * Set vCloud SDK Extension object
     * @throws ABaseVCloudServiceExtensionCreateException
     */
    private function setVCloudExtension(): void {

        $this->vCloudExtension = $this->vCloudService->createSDKExtensionObj();
        if ($this->vCloudExtension === null)
            throw new ABaseVCloudServiceExtensionCreateException("Can't create SDK Extension object ");
    }

    /**
     * Get SDK by HREF
     * @param string $href
     * @return mixed
     */
    protected function getSDKByHref(string $href) {
        return $this->vCloudService->createSDKObj($href);
    }

    /**
     * Generate the reference from href
     * @param string $href
     * @param string|null $name
     * @param string|null $tag
     * @param string|null $type
     * @return VMware_VCloud_API_ReferenceType
     */
    protected function getReferenceByHref(string $href, ?string $name, ?string $tag, ?string $type) : VMware_VCloud_API_ReferenceType {
        return VMware_VCloud_SDK_Helper::createReferenceTypeObj($href, $tag, $type, $name);
    }

    /**
     * @param string $type
     * @param string|null $fields - name,category
     * @param string|null $filter - (name==vApp*;category==Catalog%20Item)
     * @return array<VMware_VCloud_API_QueryResultOrgRecordType> or [] if not found
     */
    protected function getResultsByQuery(string $type, ?string $fields, ?string $filter) : array {

        $query = VMware_VCloud_SDK_Query::getInstance($this->vCloudService);
        $queryParameters = new VMware_VCloud_SDK_Query_Params();
        $queryParameters->setPageSize(125);

        if ($fields !== null) $queryParameters->setFields($fields);
        if ($filter !== null) $queryParameters->setFilter(str_replace(' ', '%20', $filter));

        $result = $query->queryRecords($type, $queryParameters)->getRecord();
        return $result;
    }

    /**
     * @param string $type
     * @param string|null $fields - name,category
     * @param string|null $filter - (name==vApp*;category==Catalog%20Item)
     * @return array<VMware_VCloud_API_ReferenceType> or [] if not found
     */
    protected function getReferencesByQuery(string $type, ?string $fields, ?string $filter) : array {

        $query = VMware_VCloud_SDK_Query::getInstance($this->vCloudService);
        $queryParameters = new VMware_VCloud_SDK_Query_Params();
        $queryParameters->setPageSize(125);

        if ($fields !== null) $queryParameters->setFields($fields);
        if ($filter !== null) $queryParameters->setFilter(str_replace(' ', '%20', $filter));

        $result = $query->queryReferences($type, $queryParameters)->getReference();
        return $result;
    }
}

/**
 * Class BaseVCloudServiceNotFoundInCache
 * @package Library\Sources\Models\VCloud
 */
class ABaseVCloudServiceNotFoundInCache extends Exception {};

/**
 * Class ABaseVCloudServiceAdminCreateException
 * @package Library\Sources\Models\VCloud
 */
class ABaseVCloudServiceAdminCreateException extends Exception {};

/**
 * Class ABaseVCloudServiceExtensionCreateException
 * @package Library\Sources\Models\VCloud
 */
class ABaseVCloudServiceExtensionCreateException extends Exception {};

/**
 * Class ABaseVCloudInputException
 * @package Library\Sources\Models\VCloud
 */
class ABaseVCloudInputException extends Exception {
    private $input;
    public function __construct(string $message, ?string $input = null) {
        parent::__construct($message, 0, null);
        $this->input = $input;
    }
};
