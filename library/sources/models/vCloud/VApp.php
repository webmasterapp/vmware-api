<?php

namespace Library\Sources\Models\VCloud;
use Library\Sources\Entities\VCloudVApp;
use VMware_VCloud_API_ComposeVAppParamsType;
use VMware_VCloud_API_OwnerType;
use VMware_VCloud_API_QueryResultAdminVAppRecordType;
use VMware_VCloud_API_ReferenceType;
use VMware_VCloud_API_VAppType;
use VMware_VCloud_SDK_Exception;

/**
 * Class VApp
 * @package Library\Sources\Models\VCloud
 */
class VApp extends ABaseVCloud {

    /** @var string  */
    private const clientTypeName = "adminVApp";

    /** @var string  */
    private const selectingQuery = "name,org";

    /**
     * @param string $name - name of the VApp
     * @return VMware_VCloud_API_QueryResultAdminVAppRecordType
     * @throws VAppNotFoundNameException
     * @throws VAppWrongTypeReturnedException
     */
    public function getVAppRecordByName(string $name) : VMware_VCloud_API_QueryResultAdminVAppRecordType {

        $result = $this->getResultsByQuery(self::clientTypeName, self::selectingQuery, "name==$name");
        if (count($result) === 0) throw new VAppNotFoundNameException("Can't find VApp: $name");
        if (!($result[0] instanceof VMware_VCloud_API_QueryResultAdminVAppRecordType))
            throw new VAppWrongTypeReturnedException("Wrong type returned for VApp: $name");
        return $result[0];
    }

    /**
     * @param string $name - name of the VApp
     * @return VMware_VCloud_API_ReferenceType
     * @throws VAppNotFoundNameException
     * @throws VAppWrongTypeReturnedException
     */
    public function getVAppReferenceByName(string $name) : VMware_VCloud_API_ReferenceType {

        $result = $this->getReferencesByQuery(self::clientTypeName, self::selectingQuery, "name==$name");
        if (count($result) === 0) throw new VAppNotFoundNameException("Can't find VApp: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ReferenceType))
            throw new VAppWrongTypeReturnedException("Wrong type returned for VApp: $name");
        return $result[0];
    }

    /**
     * @param VCloudVApp $cloudVApp
     * @return VMware_VCloud_API_VAppType
     * @throws VClientCreateClientException
     */
    public function createVAppFromEntity(VCloudVApp $cloudVApp) : VMware_VCloud_API_VAppType {

        $vAppParams = new VMware_VCloud_API_ComposeVAppParamsType();
        $vAppParams->set_name($cloudVApp->name);

        // VMware_VCloud_SDK_AdminVdc -> VMware_VCloud_SDK_Vdc
        $vdcSDK = $this->getSDKByHref($cloudVApp->vdc->get_href())->getSdkVdc();

        // Create the vApp
        $result = null;
        try {/** @noinspection PhpUndefinedMethodInspection */
            $result = $vdcSDK->composeVApp($vAppParams);}
            /** @noinspection PhpRedundantCatchClauseInspection */
        catch (VMware_VCloud_SDK_Exception $exception) {
            throw new VClientCreateClientException("Exception while creating vApp",
                json_encode($cloudVApp));
        }
        if (!($result instanceof VMware_VCloud_API_VAppType))
            throw new VClientCreateClientException("Problem while creating vApp",
                json_encode($cloudVApp));
        return $result;
    }

    /**
     * @param VMware_VCloud_API_ReferenceType $vApp
     * @param VMware_VCloud_API_ReferenceType $client
     * @throws VClientCreateClientException
     */
    public function setOwnerForVApp(VMware_VCloud_API_ReferenceType $vApp, VMware_VCloud_API_ReferenceType $client) : void {

        // VMware_VCloud_SDK_VApp
        $vAppSDK = $this->getSDKByHref($vApp->get_href());

        $owner = new VMware_VCloud_API_OwnerType();
        $owner->setUser($client);

        try {/** @noinspection PhpUndefinedMethodInspection */
            $vAppSDK->changeOwner($owner); }
            /** @noinspection PhpRedundantCatchClauseInspection */
        catch (VMware_VCloud_SDK_Exception $exception) {
            throw new VClientCreateClientException("Exception while creating vApp", json_encode([$vApp,$client]));
        }
    }
}

/**
 * Class VAppAlreadyExistsException
 * @package Library\Sources\Models\VCloud
 */
class VAppAlreadyExistsException extends ABaseVCloudInputException {};

/**
 * Class VAppCreateVAppException
 * @package Library\Sources\Models\VCloud
 */
class VAppCreateVAppException extends ABaseVCloudInputException {};

/**
 * Class VAppCreateVAppException
 * @package Library\Sources\Models\VCloud
 */
class VAppSetExternalNetworkToVAppException extends ABaseVCloudInputException {};

/**
 * Class VAppNotFoundNameException
 * @package Library\Sources\Models\VCloud
 */
class VAppNotFoundNameException extends  ABaseVCloudInputException {};

/**
 * Class VAppWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VAppWrongTypeReturnedException extends  ABaseVCloudInputException {};