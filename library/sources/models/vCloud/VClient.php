<?php

namespace Library\Sources\Models\VCloud;
use Library\Sources\Entities\VCloudClient;
use VMware_VCloud_API_QueryResultAdminUserRecordType;
use VMware_VCloud_API_ReferenceType;
use VMware_VCloud_API_UserType;
use VMware_VCloud_SDK_Exception;

/**
 * Class VClient
 * @package Library\Sources\Models\VCloud
 */
class VClient extends ABaseVCloud {

    /** @var string  */
    private const clientTypeName = "adminUser";

    /** @var string  */
    private const selectingQuery = "name,org";

    /**
     * @param string $name - name of the client
     * @return VMware_VCloud_API_QueryResultAdminUserRecordType
     * @throws VClientNotFoundNameException
     * @throws VClientWrongTypeReturnedException
     */
    public function getClientRecordByName(string $name) : VMware_VCloud_API_QueryResultAdminUserRecordType {

        $result = $this->getResultsByQuery(self::clientTypeName, self::selectingQuery, "name==$name");
        if (count($result) === 0) throw new VClientNotFoundNameException("Can't find client: $name");
        if (!($result[0] instanceof VMware_VCloud_API_QueryResultAdminUserRecordType))
            throw new VClientWrongTypeReturnedException("Wrong type returned for client: $name");
        return $result[0];
    }

    /**
     * @param string $name - name of the client
     * @return VMware_VCloud_API_ReferenceType
     * @throws VClientNotFoundNameException
     * @throws VClientWrongTypeReturnedException
     */
    public function getClientReferenceByName(string $name) : VMware_VCloud_API_ReferenceType {

        $result = $this->getReferencesByQuery(self::clientTypeName, self::selectingQuery, "name==$name");
        if (count($result) === 0) throw new VClientNotFoundNameException("Can't find client: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ReferenceType))
            throw new VClientWrongTypeReturnedException("Wrong type returned for client: $name");
        return $result[0];
    }

    /**
     * Create organization from entity object
     * @param VCloudClient $cloudClient
     * @return VMware_VCloud_API_UserType
     * @throws VClientAlreadyExistsException
     * @throws VClientWrongTypeReturnedException
     * @throws VMware_VCloud_API_UserType
     * @throws VClientCreateClientException
     * @throws \Library\Sources\Entities\ABaseVCloudEntityRequiredParameterMissingException
     */
    public function createClientFromEntity(VCloudClient $cloudClient) : VMware_VCloud_API_UserType {

        // Check already exist
        $clientExists = true;
        $exceptionText = "Client already exist with name: ".$cloudClient->name;
        try {$this->getClientReferenceByName($cloudClient->name);}
        catch (VClientNotFoundNameException $e) {$clientExists = false;}
        if ($clientExists) throw new VClientAlreadyExistsException($exceptionText);

        $cloudClient->check();
        $client = new VMware_VCloud_API_UserType();
        $client->set_name($cloudClient->name);
        $client->setFullName($cloudClient->fullName);
        $client->setPassword($cloudClient->password);
        $client->setRole($cloudClient->role);
        $client->setIsEnabled($cloudClient->enabled);

        // Get VMware_VCloud_SDK_AdminOrg
        $organizationSDK = $this->getSDKByHref($cloudClient->organization->get_href());

        // Create the client
        $result = null;
        try {$result = $organizationSDK->createUser($client);}
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (VMware_VCloud_SDK_Exception $exception) {
            throw new VClientCreateClientException("Exception while creating client",
                json_encode($cloudClient));
        }
        if (!($result instanceof VMware_VCloud_API_UserType))
            throw new VClientCreateClientException("Problem while creating client",
                json_encode($cloudClient));
        return $result;
    }
}

/**
 * Class VClientAlreadyExistsException
 * @package Library\Sources\Models\VCloud
 */
class VClientAlreadyExistsException extends ABaseVCloudInputException {};

/**
 * Class VClientCreateClientException
 * @package Library\Sources\Models\VCloud
 */
class VClientCreateClientException extends ABaseVCloudInputException {};

/**
 * Class VClientNotFoundOrganizationNameException
 * @package Library\Sources\Models\VCloud
 */
class VClientNotFoundNameException extends  ABaseVCloudInputException {};

/**
 * Class VClientWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VClientWrongTypeReturnedException extends  ABaseVCloudInputException {};