<?php

namespace Library\Sources\Models\VCloud;
use Library\Sources\Entities\ABaseVCloudEntityRequiredParameterMissingException;
use Library\Sources\Entities\VCloudVDC;
use Library\Sources\Entities\VCloudVDCNetwork;
use VMware_VCloud_API_AdminVdcType;
use VMware_VCloud_API_CapacityWithUsageType;
use VMware_VCloud_API_ComputeCapacityType;
use VMware_VCloud_API_CreateVdcParamsType;
use VMware_VCloud_API_NetworkConfigurationType;
use VMware_VCloud_API_OrgVdcNetworkType;
use VMware_VCloud_API_QueryResultAdminVdcRecordType;
use VMware_VCloud_API_ReferenceType;
use VMware_VCloud_API_VdcStorageProfileParamsType;
use VMware_VCloud_SDK_Exception;

/**
 * Class VDC
 * @package Library\Sources\Models\VCloud
 */
class VDC extends ABaseVCloud {

    /** @var string  */
    private const clientTypeName = "adminOrgVdc";

    /** @var string  */
    private const selectingQuery = "name,org";

    /**
     * @param string $name - name of the VDC
     * @return VMware_VCloud_API_QueryResultAdminVdcRecordType
     * @throws VDCNotFoundNameException
     * @throws VDCWrongTypeReturnedException
     */
    public function getVDCRecordByName(string $name) : VMware_VCloud_API_QueryResultAdminVdcRecordType {

        $result = $this->getResultsByQuery(self::clientTypeName, self::selectingQuery, "name==$name");
        if (count($result) === 0) throw new VDCNotFoundNameException("Can't find VDC: $name");
        if (!($result[0] instanceof VMware_VCloud_API_QueryResultAdminVdcRecordType))
            throw new VDCWrongTypeReturnedException("Wrong type returned for VDC: $name");
        return $result[0];
    }

    /**
     * @param string $name - name of the VDC
     * @return VMware_VCloud_API_ReferenceType
     * @throws VDCNotFoundNameException
     * @throws VDCWrongTypeReturnedException
     */
    public function getVDCReferenceByName(string $name) : VMware_VCloud_API_ReferenceType {

        $result = $this->getReferencesByQuery(self::clientTypeName, self::selectingQuery, "name==$name");
        if (count($result) === 0) throw new VDCNotFoundNameException("Can't find VDC: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ReferenceType))
            throw new VDCWrongTypeReturnedException("Wrong type returned for VDC: $name");
        return $result[0];
    }

    /**
     * @param VCloudVDC $cloudVDC
     * @return VMware_VCloud_API_AdminVdcType
     * @throws ABaseVCloudEntityRequiredParameterMissingException
     * @throws VDCAlreadyExistsException
     * @throws VDCCreateVDCException
     * @throws VDCWrongTypeReturnedException
     * @todo more storage profiles
     */
    public function createVDCFromEntity(VCloudVDC $cloudVDC) : VMware_VCloud_API_AdminVdcType {

        // Check already exist
        $vdcExists = true;
        $exceptionText = "VDC already exist with name: ".$cloudVDC->name;
        try {$this->getVDCReferenceByName($cloudVDC->name);}
        catch (VDCNotFoundNameException $e) {$vdcExists = false;}
        if ($vdcExists) throw new VDCAlreadyExistsException($exceptionText);

        $cloudVDC->check();

        $storageProfile = new VMware_VCloud_API_VdcStorageProfileParamsType();
        $storageProfile->setDefault($cloudVDC->storageProviderDefault);
        $storageProfile->setEnabled($cloudVDC->storageProfileIsEnabled);
        $storageProfile->setProviderVdcStorageProfile($cloudVDC->storageProfile);
        $storageProfile->setLimit($cloudVDC->storageProfileLimit);
        $storageProfile->setUnits('MB');

        $ram = new VMware_VCloud_API_CapacityWithUsageType();
        $ram->setUnits('MB');
        $ram->setAllocated($cloudVDC->ramAllocated);
        $ram->setLimit($cloudVDC->ramLimit);

        $cpu = new VMware_VCloud_API_CapacityWithUsageType();
        $cpu->setUnits('MHz');
        $cpu->setAllocated($cloudVDC->cpuAllocated);
        $cpu->setLimit($cloudVDC->cpuLimit);

        $compute = new VMware_VCloud_API_ComputeCapacityType();
        $compute->setCpu($cpu);
        $compute->setMemory($ram);

        $vdc = new VMware_VCloud_API_CreateVdcParamsType();
        $vdc->set_name($cloudVDC->name);
        $vdc->setDescription($cloudVDC->description);
        $vdc->setAllocationModel($cloudVDC->allocationModel);
        $vdc->setComputeCapacity($compute);
        $vdc->setVdcStorageProfile([$storageProfile]);
        $vdc->setResourceGuaranteedCpu($cloudVDC->resourceGuaranteedCpu);
        $vdc->setResourceGuaranteedMemory($cloudVDC->resourceGuaranteedMemory);
        $vdc->setNicQuota($cloudVDC->nicQuota);
        $vdc->setNetworkQuota($cloudVDC->networkQuota);
        $vdc->setIsEnabled($cloudVDC->isEnabled);
        $vdc->setIsThinProvision($cloudVDC->isThinProvision);
        $vdc->setUsesFastProvisioning($cloudVDC->usesFastProvisioning);
        $vdc->setProviderVdcReference($cloudVDC->provider);

        // Get VMware_VCloud_SDK_AdminOrg
        $organizationSDK = $this->getSDKByHref($cloudVDC->organization->get_href());

        // Create the client
        $result = null;
        try {$result = $organizationSDK->createAdminOrgVdc($vdc);}
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (VMware_VCloud_SDK_Exception $exception) {
            throw new VDCCreateVDCException("Exception while creating client: ",json_encode($cloudVDC));
        }
        if (!($result instanceof VMware_VCloud_API_AdminVdcType))
            throw new VDCCreateVDCException("Problem while creating client: ",json_encode($cloudVDC));
        return $result;
    }

    /**
     * @param VMware_VCloud_API_ReferenceType $externalNetwork
     * @param VMware_VCloud_API_ReferenceType $vdc
     * @param VCloudVDCNetwork $cloudVDCNetwork
     * @return VMware_VCloud_API_OrgVdcNetworkType
     * @throws VDCSetExternalNetworkToVDCException
     */
    public function setNetworkForVDC(VMware_VCloud_API_ReferenceType $externalNetwork, VMware_VCloud_API_ReferenceType $vdc,
    VCloudVDCNetwork $cloudVDCNetwork) : VMware_VCloud_API_OrgVdcNetworkType {

        $conf = new VMware_VCLoud_API_NetworkConfigurationType();
        $conf->setParentNetwork($externalNetwork);
        $conf->setFenceMode($cloudVDCNetwork->fenceMode);

        $network = new VMware_VCloud_API_OrgVdcNetworkType();
        $network->set_name($cloudVDCNetwork->name);
        $network->setConfiguration($conf);

        // VMware_VCloud_SDK_AdminVdc
        $vdcSDK = $this->getSDKByHref($vdc->get_href());

        // Set the network
        $result = null;
        try {$result = $vdcSDK->addvdcNetwork($network);}
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (VMware_VCloud_SDK_Exception $exception) {
            throw new VDCSetExternalNetworkToVDCException("Exception while setting external network",
                json_encode([$externalNetwork,$vdc]));
        }
        if (!($result instanceof VMware_VCloud_API_OrgVdcNetworkType))
            throw new VDCSetExternalNetworkToVDCException("Problem while setting external network",
                json_encode([$externalNetwork,$vdc]));
        return $result;
    }
}

/**
 * Class VDCAlreadyExistsException
 * @package Library\Sources\Models\VCloud
 */
class VDCAlreadyExistsException extends ABaseVCloudInputException {};

/**
 * Class VDCCreateVDCException
 * @package Library\Sources\Models\VCloud
 */
class VDCCreateVDCException extends ABaseVCloudInputException {};

/**
 * Class VDCCreateVDCException
 * @package Library\Sources\Models\VCloud
 */
class VDCSetExternalNetworkToVDCException extends ABaseVCloudInputException {};

/**
 * Class VDCNotFoundNameException
 * @package Library\Sources\Models\VCloud
 */
class VDCNotFoundNameException extends  ABaseVCloudInputException {};

/**
 * Class VDCWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VDCWrongTypeReturnedException extends  ABaseVCloudInputException {};