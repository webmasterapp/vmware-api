<?php

namespace Library\Sources\Models\VCloud;
use VMware_VCloud_API_Extension_VMWExternalNetworkType;
use VMware_VCloud_API_ReferenceType;

/**
 * Class VExternalNetwork
 * @package Library\Sources\Models\VCloud
 */
class VExternalNetwork extends ABaseVCloud {

    /**
     * @param string $name - name of the external network
     * @return VMware_VCloud_API_Extension_VMWExternalNetworkType
     * @throws VExternalNetworkNotFoundOrganizationNameException
     * @throws VExternalNetworkWrongTypeReturnedException
     */
    public function getExternalNetworkRecordByName(string $name) : VMware_VCloud_API_Extension_VMWExternalNetworkType {

        $result = $this->vCloudExtension->getVMWExternalNetworks($name);
        if (count($result) === 0) throw new VExternalNetworkNotFoundOrganizationNameException("Can't find external network: $name");
        if (!($result[0] instanceof VMware_VCloud_API_Extension_VMWExternalNetworkType))
            throw new VExternalNetworkWrongTypeReturnedException("Wrong type returned for external network: $name");
        return $result[0];
    }

    /**
     * @param string $name
     * @return VMware_VCloud_API_ReferenceType
     * @throws VExternalNetworkNotFoundOrganizationNameException
     * @throws VExternalNetworkWrongTypeReturnedException
     */
    public function getExternalNetworkReferenceByName(string $name) : VMware_VCloud_API_ReferenceType {

        $result = $this->vCloudExtension->getVMWExternalNetworkRefs($name);
        if (count($result) === 0) throw new VExternalNetworkNotFoundOrganizationNameException("Can't find external network: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ReferenceType))
            throw new VExternalNetworkWrongTypeReturnedException("Wrong type returned for external network: $name");
        return $result[0];
    }
}

/**
 * Class VExternalNetworkNotFoundOrganizationNameException
 * @package Library\Sources\Models\VCloud
 */
class VExternalNetworkNotFoundOrganizationNameException extends  ABaseVCloudInputException {};

/**
 * Class VExternalNetworkWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VExternalNetworkWrongTypeReturnedException extends  ABaseVCloudInputException {};