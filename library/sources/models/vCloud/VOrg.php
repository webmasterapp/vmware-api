<?php

namespace Library\Sources\Models\VCloud;
use Library\Sources\Entities\VCloudOrganization;
use VMware_VCloud_API_AdminOrgType;
use VMware_VCloud_API_LinkType;
use VMware_VCloud_API_OrgGeneralSettingsType;
use VMware_VCloud_API_OrgLdapSettingsType;
use VMware_VCloud_API_OrgLeaseSettingsType;
use VMware_VCloud_API_OrgSettingsType;
use VMware_VCloud_API_OrgVAppTemplateLeaseSettingsType;
use VMware_VCloud_API_ReferenceType;
use VMware_VCloud_SDK_Exception;

/**
 * Class VOrg
 * @package Library\Sources\Models\VCloud
 */
class VOrg extends ABaseVCloud {

    /**
     * @param string $name - name of the organization
     * @return VMware_VCloud_API_AdminOrgType
     * @throws VOrgNotFoundOrganizationNameException
     * @throws VOrgWrongTypeReturnedException
     */
    public function getOrganizationRecordByName(string $name) : VMware_VCloud_API_AdminOrgType {

        $result = $this->vCloudAdmin->getAdminOrgs($name);
        if (count($result) === 0) throw new VOrgNotFoundOrganizationNameException("Can't find organization: $name");
        if (!($result[0] instanceof VMware_VCloud_API_AdminOrgType))
            throw new VOrgWrongTypeReturnedException("Wrong type returned for organization: $name");
        return $result[0];
    }

    /**
     * @param string $name
     * @return VMware_VCloud_API_ReferenceType
     * @throws VOrgNotFoundOrganizationNameException
     * @throws VOrgWrongTypeReturnedException
     */
    public function getOrganizationReferenceByName(string $name) : VMware_VCloud_API_ReferenceType {

        $result = $this->vCloudAdmin->getAdminOrgRefs($name);
        if (count($result) === 0) throw new VOrgNotFoundOrganizationNameException("Can't find organization: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ReferenceType))
            throw new VOrgWrongTypeReturnedException("Wrong type returned for organization: $name");
        return $result[0];
    }

    /**
     * Create organization from entity object
     * @param VCloudOrganization $cloudOrganization
     * @return VMware_VCloud_API_AdminOrgType|null
     * @throws VOrgAlreadyExistsException
     * @throws VOrgCreateOrganizationException
     * @throws VOrgWrongTypeReturnedException
     * @throws \Library\Sources\Entities\ABaseVCloudEntityRequiredParameterMissingException
     */
    public function createOrganizationFromEntity(VCloudOrganization $cloudOrganization) {

        // Check already exist
        $organizationExists = true;
        $exceptionText = "Organization already exist with name: ".$cloudOrganization->name;
        try {$this->getOrganizationRecordByName($cloudOrganization->name);}
        catch (VOrgNotFoundOrganizationNameException $e) {$organizationExists = false;}
        if ($organizationExists) throw new VOrgAlreadyExistsException($exceptionText);

        // Initialize the setting classes
        $cloudOrganization->check();
        $generalSettings = new VMware_VCloud_API_OrgGeneralSettingsType();
        $ldapSettings = new VMware_VCloud_API_OrgLdapSettingsType();
        $leaseSettings = new VMware_VCloud_API_OrgLeaseSettingsType();
        $tempLeaseSettings = new VMware_VCloud_API_OrgVAppTemplateLeaseSettingsType();
        $settings = new VMware_VCloud_API_OrgSettingsType();
        $admin = new VMware_VCloud_API_AdminOrgType();

        // Set the settings
        $generalSettings->setCanPublishCatalogs($cloudOrganization->canPublishCatalogs);
        $ldapSettings->setOrgLdapMode($cloudOrganization->orgLdapMode);
        $leaseSettings->setStorageLeaseSeconds($cloudOrganization->storageLeaseSeconds);
        $leaseSettings->setDeploymentLeaseSeconds($cloudOrganization->deploymentLeaseSeconds);
        $leaseSettings->setDeleteOnStorageLeaseExpiration($cloudOrganization->deleteOnStorageLeaseExpiration);
        $tempLeaseSettings->setStorageLeaseSeconds($cloudOrganization->storageLeaseSeconds);
        $tempLeaseSettings->setDeleteOnStorageLeaseExpiration($cloudOrganization->deleteOnStorageLeaseExpiration);
        $settings->setOrgGeneralSettings($generalSettings);
        $settings->setOrgLdapSettings($ldapSettings);
        $settings->setVAppLeaseSettings($leaseSettings);
        $settings->setVAppTemplateLeaseSettings($tempLeaseSettings);
        $admin->set_name($cloudOrganization->name);
        $admin->setFullName($cloudOrganization->fullName);
        $admin->setDescription($cloudOrganization->description);
        $admin->setSettings($settings);
        $admin->setIsEnabled(true);

        // Create the organization
        $result = null;
        try {$result = $this->vCloudAdmin->createOrganization($admin);}
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (VMware_VCloud_SDK_Exception $exception) {
            throw new VOrgCreateOrganizationException("Exception while creating organization",
                json_encode($cloudOrganization));
        }
        if (!($result instanceof VMware_VCloud_API_AdminOrgType))
            throw new VOrgCreateOrganizationException("Problem while creating organization",
                json_encode($cloudOrganization));
        return $result;
    }

    /**
     * @param VMware_VCloud_API_ReferenceType $organization
     * @param string $name
     * @return VMware_VCloud_API_LinkType
     * @throws VOrgMoreNetworksInOrganizationFoundException
     * @throws VOrgWrongTypeConvertedException
     * @throws VTemplateWrongTypeReturnedException
     */
    public function getNetworkRefByName(VMware_VCloud_API_ReferenceType $organization, string $name)
    : VMware_VCloud_API_ReferenceType {

        // VMware_VCloud_SDK_AdminOrg -> VMware_VCloud_SDK_Org
        $organizationSDK = $this->getSDKByHref($this->getSDKByHref($organization->get_href())->getOrgRef()->get_href());

        $result = $organizationSDK->getOrgNetworkRefs($name);
        if (count($result) !== 1) throw new VOrgMoreNetworksInOrganizationFoundException("More networks in organization",
            json_encode([$organization,$name]));
        if (!($result[0] instanceof VMware_VCloud_API_LinkType))
            throw new VTemplateWrongTypeReturnedException("Wrong type returned for network from organization",
                json_encode([$organization,$name]));

        $result = $this->getReferenceByHref($result[0]->get_href(), $result[0]->get_name(), null, $result[0]->get_type());
        if (!($result instanceof VMware_VCloud_API_ReferenceType))
            throw new VOrgWrongTypeConvertedException("Wrong type converted for network from organization",
                json_encode([$organization,$name]));

        return $result;
    }
}

/**
 * Class VOrgAlreadyExistsException
 * @package Library\Sources\Models\VCloud
 */
class VOrgAlreadyExistsException extends ABaseVCloudInputException {};

/**
 * Class VOrgCreateOrganizationException
 * @package Library\Sources\Models\VCloud
 */
class VOrgCreateOrganizationException extends ABaseVCloudInputException {};

/**
 * Class VOrgNotFoundOrganizationNameException
 * @package Library\Sources\Models\VCloud
 */
class VOrgNotFoundOrganizationNameException extends ABaseVCloudInputException {};

/**
 * Class VOrgWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VOrgWrongTypeReturnedException extends  ABaseVCloudInputException {};

/**
 * Class VOrgWrongTypeConvertedException
 * @package Library\Sources\Models\VCloud
 */
class VOrgWrongTypeConvertedException extends  ABaseVCloudInputException {};

/**
 * Class VOrgMoreNetworksInOrganizationFoundException
 * @package Library\Sources\Models\VCloud
 */
class VOrgMoreNetworksInOrganizationFoundException extends  ABaseVCloudInputException {};