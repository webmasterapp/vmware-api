<?php

namespace Library\Sources\Models\VCloud;
use VMware_VCloud_API_ProviderVdcType;
use VMware_VCloud_API_ReferenceType;

/**
 * Class VProvider
 * @package Library\Sources\Models\VCloud
 */
class VProvider extends ABaseVCloud {

    /**
     * @param string $name - name of the provider
     * @return VMware_VCloud_API_ProviderVdcType
     * @throws VProviderNotFoundOrganizationNameException
     * @throws VProviderWrongTypeReturnedException
     */
    public function getProviderRecordByName(string $name) : VMware_VCloud_API_ProviderVdcType {

        $result = $this->vCloudAdmin->getProviderVdcs($name);
        if (count($result) === 0) throw new VProviderNotFoundOrganizationNameException("Can't find provider: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ProviderVdcType))
            throw new VProviderWrongTypeReturnedException("Wrong type returned for provider: $name");
        return $result[0];
    }

    /**
     * @param string $name - name of the provider
     * @return VMware_VCloud_API_ReferenceType
     * @throws VProviderNotFoundOrganizationNameException
     * @throws VProviderWrongTypeReturnedException
     */
    public function getProviderReferenceByName(string $name) : VMware_VCloud_API_ReferenceType {

       $result = $this->vCloudAdmin->getProviderVdcRefs($name);
        if (count($result) === 0) throw new VProviderNotFoundOrganizationNameException("Can't find provider: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ReferenceType))
            throw new VProviderWrongTypeReturnedException("Wrong type returned for provider: $name");
        return $result[0];
    }

    /**
     * @param string $name
     * @param VMware_VCloud_API_ReferenceType $provider
     * @return VMware_VCloud_API_ReferenceType
     * @throws VProviderNotFoundOrganizationNameException
     * @throws VProviderWrongTypeReturnedException
     */
    public function getStorageProfileReferenceByName(string $name, VMware_VCloud_API_ReferenceType $provider) : VMware_VCloud_API_ReferenceType {

        $providerSDK = $this->getSDKByHref($provider->get_href());
        $result = $providerSDK->getProviderVdcStorageProfileRefs();
        if (count($result) === 0) throw new VProviderNotFoundOrganizationNameException("Can't find profile: $name");

        foreach ($result as $profile) {
            if (!($profile instanceof VMware_VCloud_API_ReferenceType))
                throw new VProviderWrongTypeReturnedException("Wrong type returned for profile: $name");
            if ($profile->get_name() === $name) return $profile;
        }

        throw new VProviderNotFoundOrganizationNameException("Can't find profile: $name");
    }
}

/**
 * Class VProviderNotFoundOrganizationNameException
 * @package Library\Sources\Models\VCloud
 */
class VProviderNotFoundOrganizationNameException extends  ABaseVCloudInputException {};

/**
 * Class VProviderWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VProviderWrongTypeReturnedException extends  ABaseVCloudInputException {};