<?php

namespace Library\Sources\Models\VCloud;
use VMware_VCloud_API_ReferenceType;
use VMware_VCloud_API_RoleType;

/**
 * Class VRole
 * @package Library\Sources\Models\VCloud
 */
class VRole extends ABaseVCloud {

    /**
     * @param string $name - name of the role
     * @return VMware_VCloud_API_RoleType
     * @throws VOrgWrongTypeReturnedException
     * @throws VRoleNotFoundOrganizationNameException
     */
    public function getRoleRecordByName(string $name) : VMware_VCloud_API_RoleType {

        $result = $this->vCloudAdmin->getRoles($name);
        if (count($result) === 0) throw new VRoleNotFoundOrganizationNameException("Can't find organization: $name");
        if (!($result[0] instanceof VMware_VCloud_API_RoleType))
            throw new VOrgWrongTypeReturnedException("Wrong type returned for organization: $name");
        return $result[0];
    }

    /**
     * @param string $name - name of the role
     * @return VMware_VCloud_API_ReferenceType
     * @throws VOrgWrongTypeReturnedException
     * @throws VRoleNotFoundOrganizationNameException
     */
    public function getRoleReferenceByName(string $name) : VMware_VCloud_API_ReferenceType {

       $result = $this->vCloudAdmin->getRoleRefs($name);
        if (count($result) === 0) throw new VRoleNotFoundOrganizationNameException("Can't find role: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ReferenceType))
            throw new VOrgWrongTypeReturnedException("Wrong type returned for organization: $name");
        return $result[0];
    }
}

/**
 * Class VRoleNotFoundOrganizationNameException
 * @package Library\Sources\Models\VCloud
 */
class VRoleNotFoundOrganizationNameException extends  ABaseVCloudInputException {};

/**
 * Class VRoleWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VRoleWrongTypeReturnedException extends  ABaseVCloudInputException {};