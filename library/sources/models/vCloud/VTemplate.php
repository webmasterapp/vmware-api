<?php

namespace Library\Sources\Models\VCloud;
use VMware_VCloud_API_QueryResultVAppTemplateRecordType;
use VMware_VCloud_API_ReferenceType;
use VMware_VCloud_API_VAppTemplateType;

/**
 * Class VTemplate
 * @package Library\Sources\Models\VCloud
 */
class VTemplate extends ABaseVCloud {

    /** @var string  */
    private const typeName = "vAppTemplate";

    /** @var string  */
    private const selectingQuery = "name,org";

    /**
     * @param string $name - name of the role
     * @return VMware_VCloud_API_QueryResultVAppTemplateRecordType
     * @throws VTemplateNotFoundNameException
     * @throws VTemplateWrongTypeReturnedException
     */
    public function getTemplateRecordByName(string $name) : VMware_VCloud_API_QueryResultVAppTemplateRecordType {

        $result = $this->getResultsByQuery(self::typeName, self::selectingQuery, "name==$name");
        if (count($result) === 0) throw new VTemplateNotFoundNameException("Can't find template: $name");
        if (!($result[0] instanceof VMware_VCloud_API_QueryResultVAppTemplateRecordType))
            throw new VTemplateWrongTypeReturnedException("Wrong type returned for template: $name");
        return $result[0];
    }

    /**
     * @param string $name - name of the role
     * @return VMware_VCloud_API_ReferenceType
     * @throws VOrgWrongTypeReturnedException
     * @throws VTemplateNotFoundNameException
     */
    public function getTemplateReferenceByName(string $name) : VMware_VCloud_API_ReferenceType {

        $result = $this->getReferencesByQuery(self::typeName, self::selectingQuery, "name==$name");
        if (count($result) === 0) throw new VTemplateNotFoundNameException("Can't find template: $name");
        if (!($result[0] instanceof VMware_VCloud_API_ReferenceType))
            throw new VOrgWrongTypeReturnedException("Wrong type returned for template: $name");
        return $result[0];
    }

    /**
     * @param VMware_VCloud_API_ReferenceType $template
     * @return VMware_VCloud_API_ReferenceType
     * @throws VTemplateMoreVmInTemplateFoundException
     * @throws VTemplateWrongTypeReturnedException
     * @throws VTemplateWrongTypeConvertedException
     */
    public function getVAppByTemplate(VMware_VCloud_API_ReferenceType $template) : VMware_VCloud_API_ReferenceType {

        // VMware_VCloud_SDK_VAppTemplate
        $templateSDK = $this->getSDKByHref($template->get_href());

        $result = $templateSDK->getContainedVms();
        if (count($result) !== 1) throw new VTemplateMoreVmInTemplateFoundException("More VM in template", json_encode($template));
        if (!($result[0] instanceof VMware_VCloud_API_VAppTemplateType))
            throw new VTemplateWrongTypeReturnedException("Wrong type returned for vApp from template", json_encode($template));

        $result = $this->getReferenceByHref($result[0]->get_href(), $result[0]->get_name(), null, $result[0]->get_type());
        if (!($result instanceof VMware_VCloud_API_ReferenceType))
            throw new VTemplateWrongTypeConvertedException("Wrong type returned for vApp from template", json_encode($template));

        return $result;
    }
}

/**
 * Class VTemplateNotFoundNameException
 * @package Library\Sources\Models\VCloud
 */
class VTemplateNotFoundNameException extends  ABaseVCloudInputException {};

/**
 * Class VTemplateWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VTemplateWrongTypeReturnedException extends  ABaseVCloudInputException {};

/**
 * Class VTemplateMoreVmInTemplateFoundException
 * @package Library\Sources\Models\VCloud
 */
class VTemplateMoreVmInTemplateFoundException extends ABaseVCloudInputException {};

/**
 * Class VTemplateWrongTypeConvertedException
 * @package Library\Sources\Models\VCloud
 */
class VTemplateWrongTypeConvertedException extends  ABaseVCloudInputException {};
