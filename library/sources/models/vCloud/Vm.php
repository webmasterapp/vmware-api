<?php

namespace Library\Sources\Models\VCloud;
use Library\Sources\Entities\ABaseVCloudEntityRequiredParameterMissingException;
use Library\Sources\Entities\VCloudVm;
use VMware_VCloud_API_InstantiationParamsType;
use VMware_VCloud_API_NetworkConfigSectionType;
use VMware_VCloud_API_NetworkConfigurationType;
use VMware_VCloud_API_OVF_Msg_Type;
use VMware_VCloud_API_RecomposeVAppParamsType;
use VMware_VCloud_API_TaskType;
use VMware_VCloud_API_VAppNetworkConfigurationType;
use VMware_VCloud_SDK_Exception;

/**
 * Class Vm
 * @package Library\Sources\Models\VCloud
 */
class Vm extends ABaseVCloud {

    /**
     * @param VCloudVm $cloudVApp
     * @return VMware_VCloud_API_TaskType
     * @throws ABaseVCloudEntityRequiredParameterMissingException
     * @throws VmCreateException
     * @todo just small letters network name working
     */
    public function createVmFromEntity(VCloudVm $cloudVApp) : VMware_VCloud_API_TaskType {

        $cloudVApp->check();

        $cloudVApp->parentNetwork->set_tagName("ParentNetwork");
        $cloudVApp->sourceVApp->set_name($cloudVApp->name);

        $networkInfo = new VMware_VCloud_API_OVF_Msg_Type();
        $networkInfo->set_valueOf($cloudVApp->networkInfo);

        $networkConfiguration = new VMware_VCloud_API_NetworkConfigurationType();
        $networkConfiguration->setParentNetwork($cloudVApp->parentNetwork);
        $networkConfiguration->setFenceMode($cloudVApp->fenceMode);

        $vAppNetworkConfiguration = new VMware_VCloud_API_VAppNetworkConfigurationType();
        $vAppNetworkConfiguration->set_networkName($cloudVApp->networkName);
        $vAppNetworkConfiguration->setConfiguration($networkConfiguration);
        $vAppNetworkConfiguration->setIsDeployed($cloudVApp->isDeployed);

        $networkSection = new VMware_VCloud_API_NetworkConfigSectionType();
        $networkSection->setInfo($networkInfo);
        $networkSection->setNetworkConfig([$vAppNetworkConfiguration]);

        $instantiationParams = new VMware_VCloud_API_InstantiationParamsType();
        $instantiationParams->setSection([$networkSection]);

        $sourcedCompositionParams = new \VMware_VCloud_API_SourcedCompositionItemParamType();
        $sourcedCompositionParams->setSource($cloudVApp->sourceVApp);
        $sourcedCompositionParams->set_sourceDelete($cloudVApp->sourceDelete);

        $params = new VMware_VCloud_API_RecomposeVAppParamsType();
        $params->setInstantiationParams($instantiationParams);
        $params->setSourcedItem([$sourcedCompositionParams]);

        // VMware_VCloud_SDK_VApp
        $vAppSDK = $this->getSDKByHref($cloudVApp->vApp->get_href());


        $result = null;
        try {/** @noinspection PhpUndefinedMethodInspection */
            $result = $vAppSDK->recompose($params); }
            /** @noinspection PhpRedundantCatchClauseInspection */
        catch (VMware_VCloud_SDK_Exception $exception) {
            throw new VmCreateException("Exception while creating vm", json_encode([$cloudVApp]));
        }

        if (!($result instanceof VMware_VCloud_API_TaskType))
            throw new VmCreateException("Problem while creating vm",
                json_encode($cloudVApp));

        return $result;
    }
}

/**
 * Class VmCreateVAppException
 * @package Library\Sources\Models\VCloud
 */
class VmCreateVAppException extends ABaseVCloudInputException {};

/**
 * Class VmWrongTypeReturnedException
 * @package Library\Sources\Models\VCloud
 */
class VmWrongTypeReturnedException extends  ABaseVCloudInputException {};

/**
 * Class VmCreateException
 * @package Library\Sources\Models\VCloud
 */
class VmCreateException extends  ABaseVCloudInputException {};