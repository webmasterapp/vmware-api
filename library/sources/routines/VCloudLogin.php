<?php
namespace Library\Sources\Routines;
use Exception;
use Library\Sources\Services\Parameters;
use Library\Sources\Services\Reporter;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\DI\Container;
use VMware_VCloud_SDK_Service;

$app = require __DIR__ . '/../../configuration/bootstrap.php';

/**
 * Class VCloudLogin
 * @package Library\Sources\Routines
 */
class VCloudLogin {

    /** @var array */
    private $containerParameters;

    /** @var IStorage */
    private $cacheService;

    /** @var VMware_VCloud_SDK_Service */
    private $vCloudService;

    /** @var Reporter */
    private $reportService;

    /** @var string  */
    private const serviceCacheKey = "vCloudApiService";

    /**
     * VCloudLogin constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {

        $parametersService = $container->getByType(Parameters::class);
        $this->containerParameters = $parametersService->getParameters();
        $this->cacheService = $container->getByType(IStorage::class);
        $this->reportService = $container->getByType(Reporter::class);
        $this->vCloudService = VMware_VCloud_SDK_Service::getService();
    }

    /**
     * Try to save service
     * @throws VCloudLoginEmptyServiceReturned
     */
    public function saveService() : void {

        try {$this->connect();}
        catch (Exception $exception) {$this->reportService->report($exception, __CLASS__);}

        // Check
        if (empty($this->vCloudService)) throw new VCloudLoginEmptyServiceReturned("Can't get service");

        // Try to save
        $this->cacheService->write(self::serviceCacheKey, $this->vCloudService, [
            Cache::TAGS => [__CLASS__, __FUNCTION__, "VMware"]
        ]);
    }

    /**
     * Call VCloud API to get login
     * @throws VCloudLoginParameterNotFoundException
     * @throws VCloudLoginCommunicationException
     */
    private function connect() : void {

        // Prepare config
        $httpConfig = [
            'proxy_host'=>null,
            'proxy_port'=>null,
            'proxy_user'=>null,
            'proxy_password'=>null,
            'ssl_verify_peer'=>false,
            'ssl_verify_host'=>false,
            'ssl_cafile' => null
        ];

        // Check parameters
        if (!isset($this->containerParameters['vCloudApi']['server']) ||
            !isset($this->containerParameters['vCloudApi']['login']) ||
            !isset($this->containerParameters['vCloudApi']['password']) ||
            !isset($this->containerParameters['vCloudApi']['apiVersion'])
        ) throw new VCloudLoginParameterNotFoundException();

        // Try to get login
        try {
            $this->vCloudService->login($this->containerParameters['vCloudApi']['server'],
                ['username' => $this->containerParameters['vCloudApi']['login'],
                    'password' => $this->containerParameters['vCloudApi']['password']],
                $httpConfig, $this->containerParameters['vCloudApi']['apiVersion']);
        } catch (Exception $exception) {throw new VCloudLoginCommunicationException($exception->getMessage());}
    }

    /**
     * Get VCloud Service
     * @return VMware_VCloud_SDK_Service
     */
    public function getVCloudService(): VMware_VCloud_SDK_Service { return $this->vCloudService; }
}

/**
 * Class VCloudLoginParameterNotFoundException
 * @package Library\Sources\Routines
 */
class VCloudLoginParameterNotFoundException extends Exception {};

/**
 * Class VCloudLoginCommunicationException
 * @package Library\Sources\Routines
 */
class VCloudLoginCommunicationException extends Exception {};

/**
 * Class VCloudLoginEmptyServiceReturned
 * @package Library\Sources\Routines
 */
class VCloudLoginEmptyServiceReturned extends Exception {};

// Run
$run = new VCloudLogin($app);

/** @noinspection PhpUnhandledExceptionInspection */
$run->saveService();
