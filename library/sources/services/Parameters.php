<?php
namespace Library\Sources\Services;

/**
 * Class Parameters
 * Get the container parameters
 * @package App\Models\System
 */
class Parameters {

    /** @var array - the container parameters */
    private $containerParameters;

    /**
     * Parameters constructor.
     * @param array $containerParameters
     */
    public function __construct(array $containerParameters) {
        $this->containerParameters = $containerParameters;
    }

    /**
     * Getter
     * @return array - the params
     */
    public function getParameters() : array {
        return $this->containerParameters;
    }
}
