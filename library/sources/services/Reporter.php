<?php /** @noinspection PhpParamsInspection */

namespace Library\Sources\Services;

use Exception;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

/**
 * Class Reporter
 * @package Library\Sources\Services
 */
class Reporter {

    /** @var array */
    private $containerParameters;

    /**
     * Reporter constructor.
     * @param Parameters $parameters
     */
    public function __construct(Parameters $parameters) {
        $this->containerParameters = $parameters->getParameters();
    }

    /**
     * Getter
     * @param Exception $exception
     * @param string $callerClass
     */
    public function report(Exception $exception, string $callerClass) : void {
        $mail = new Message();
        $mail->setFrom("VMware API <pavelka@master.cz>")
            ->addTo($this->containerParameters['mailMaster'])
            ->setSubject($this->containerParameters['environmentName'].": ".$callerClass)
            ->setBody($exception->getMessage());

        $mailer = new SendmailMailer;
        $mailer->send($mail);
    }
}
