<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Entities\VCloudVApp;
use Library\Sources\Models\VCloud\VApp;
use Library\Sources\Models\VCloud\VClient;
use Library\Sources\Models\VCloud\VDC;
use Tester\Assert;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VAppTest
 * @testCase
 */
class VAppTest extends TestCase {

    /** @var VApp */
    private $app;

    /** @var VDC */
    private $appVdc;

    /** @var VClient */
    private $appClient;

    /** @var string */
    private const vAppName = "test6vApp";

    /** @var string */
    private const vdcName = "test6vdc";

    /** @var string  */
    private const clientName = "pavelka3";

    /**
     * VDCTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {

        $this->app = $container->getByType(VApp::class);
        $this->appVdc = $container->getByType(VDC::class);
        $this->appClient = $container->getByType(VClient::class);
    }

    public function testCreateVAppFromEntity() {

        $vApp = new VCloudVApp();
        $vApp->vdc = $this->appVdc->getVDCReferenceByName(self::vdcName);
        $vApp->name = self::vAppName;
        $result = $this->app->createVAppFromEntity($vApp);
        Assert::type("VMware_VCloud_API_VAppType",$result);
    }

    public function testGetVAppRecordByName() {

        $result = $this->app->getVAppRecordByName(self::vAppName);
        Assert::type("VMware_VCloud_API_QueryResultAdminVAppRecordType",$result);
        Assert::equal(self::vAppName, $result->get_name());
    }

    public function testGetVAppReferenceByName() {

        $result = $this->app->getVAppReferenceByName(self::vAppName);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal(self::vAppName, $result->get_name());
    }

    public function testSetOwnerForVApp() {

        $this->app->setOwnerForVApp($this->app->getVAppReferenceByName(self::vAppName),
            $this->appClient->getClientReferenceByName(self::clientName));
        Assert::equal(true,true);

    }
}

// Run the tests
(new VAppTest($container))->run();
