<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Entities\VCloudClient;
use Library\Sources\Models\VCloud\VClient;
use Library\Sources\Models\VCloud\VOrg;
use Library\Sources\Models\VCloud\VRole;
use Tester\Assert;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VClientTest
 * @testCase
 */
class VClientTest extends TestCase {

    /** @var VClient $app */
    private $app;

    /** @var VRole $app */
    private $appRole;

    /** @var VOrg $app */
    private $appOrganization;

    /** @var string  */
    private const roleName = "Organization Administrator";

    /** @var string  */
    private const organizationName = "test6";

    /** @var string  */
    private const clientName = "pavelka3";

    /**
     * VClientTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {

        $this->app = $container->getByType(VClient::class);
        $this->appRole = $container->getByType(VRole::class);
        $this->appOrganization = $container->getByType(VOrg::class);
    }

    public function testCreateClientFromEntity() {

        $client = new VCloudClient();
        $client->enabled = true;
        $client->password = "master++";
        $client->fullName = "Martin Pavelka";
        $client->name = self::clientName;
        $client->role = $this->appRole->getRoleReferenceByName(self::roleName);
        $client->organization = $this->appOrganization->getOrganizationRecordByName(self::organizationName);

        $result = $this->app->createClientFromEntity($client);
        Assert::type("VMware_VCloud_API_UserType",$result);
    }

    public function testGetClientRecordsByName() {

        $result = $this->app->getClientRecordByName(self::clientName);
        Assert::type("VMware_VCloud_API_QueryResultAdminUserRecordType",$result);
        Assert::equal(self::clientName, $result->get_name());
    }

    public function testGetClientReferenceByName() {

        $result = $this->app->getClientReferenceByName(self::clientName);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal(self::clientName, $result->get_name());
    }
}

// Run the tests
(new VClientTest($container))->run();
