<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Entities\VCloudVDC;
use Library\Sources\Entities\VCloudVDCNetwork;
use Library\Sources\Models\VCloud\VDC;
use Library\Sources\Models\VCloud\VExternalNetwork;
use Library\Sources\Models\VCloud\VOrg;
use Library\Sources\Models\VCloud\VProvider;
use Tester\Assert;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VDCTest
 * @testCase
 */
class VDCTest extends TestCase {

    /** @var VDC */
    private $app;

    /** @var VProvider */
    private $appProvider;

    /** @var VOrg */
    private $appOrganization;

    /** @var VExternalNetwork */
    private $appExternalNetwork;

    /** @var string  */
    private const vdcName = "test6vdc";

    /** @var string  */
    private const providerName = "Brno DC4 Entry";

    /** @var string  */
    private const profileName = "Entry";

    /** @var string  */
    private const organizationName = "test6";

    /** @var string  */
    private const externalNetworkName = "VLAN2004 - cloud Brno DC4";

    /**
     * VDCTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {

        $this->app = $container->getByType(VDC::class);
        $this->appProvider = $container->getByType(VProvider::class);
        $this->appOrganization = $container->getByType(VOrg::class);
        $this->appExternalNetwork = $container->getByType(VExternalNetwork::class);
    }

    public function testCreateVDCFromEntity() {

        $vdc = new VCloudVDC();
        $vdc->name = self::vdcName;
        $vdc->cpuLimit = 2048;
        $vdc->ramLimit = 1024;
        $vdc->provider = $this->appProvider->getProviderReferenceByName(self::providerName);
        $vdc->storageProfile = $this->appProvider->getStorageProfileReferenceByName(self::profileName, $vdc->provider);
        $vdc->organization = $this->appOrganization->getOrganizationReferenceByName(self::organizationName);
        $result = $this->app->createVDCFromEntity($vdc);
        Assert::type("VMware_VCloud_API_AdminVdcType", $result);
    }

    public function testGetVDCRecordByName() {

        $result = $this->app->getVDCRecordByName(self::vdcName);
        Assert::type("VMware_VCloud_API_QueryResultAdminVdcRecordType",$result);
        Assert::equal(self::vdcName, $result->get_name());
        Assert::equal(self::organizationName, $result->get_org());
    }

    public function testGetVDCReferenceByName() {

        $result = $this->app->getVDCReferenceByName(self::vdcName);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal(self::vdcName, $result->get_name());
    }

    public function testSetNetworkForVDC() {

        $vdcNetwork = new VCloudVDCNetwork();
        $externalNetwork = $this->appExternalNetwork->getExternalNetworkReferenceByName(self::externalNetworkName);
        $vdc = $this->app->getVDCReferenceByName(self::vdcName);
        $result = $this->app->setNetworkForVDC($externalNetwork, $vdc, $vdcNetwork);
        Assert::type("VMware_VCloud_API_OrgVdcNetworkType", $result);

    }
}

// Run the tests
(new VDCTest($container))->run();
