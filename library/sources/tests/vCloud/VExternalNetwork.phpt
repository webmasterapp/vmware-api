<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Models\VCloud\VExternalNetwork;
use Tester\Assert;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VExternalNetworkTest
 * @testCase
 */
class VExternalNetworkTest extends TestCase {

    /** @var VExternalNetwork $app */
    private $app;

    /** @var string  */
    private const externalNetworkName = "VLAN2004 - cloud Brno DC4";

    /**
     * VClientTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->app = $container->getByType(VExternalNetwork::class);
    }

    public function testGetExternalNetworkRecordByName() {

       $result = $this->app->getExternalNetworkRecordByName(self::externalNetworkName);
       Assert::type("VMware_VCloud_API_Extension_VMWExternalNetworkType",$result);
       Assert::equal(self::externalNetworkName, $result->get_name());
   }

    public function testGetExternalNetworkReferenceByName() {

        $result = $this->app->getExternalNetworkReferenceByName(self::externalNetworkName);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal(self::externalNetworkName, $result->get_name());
    }
}

// Run the tests
(new VExternalNetworkTest($container))->run();
