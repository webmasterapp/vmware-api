<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Models\VCloud\VOrg;
use Tester\Assert;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VOrgTest
 * @testCase
 */
class VOrgTest extends TestCase {

    /** @var VOrg $app */
    private $app;

    /** @var string  */
    private const organizationName = "test6";

    /** @var string  */
    private const networkName = "internet";

    /**
     * VOrgTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->app = $container->getByType(VOrg::class);
    }

    /*public function testCreateOrganizationFromEntity() {

        $entity = new VCloudOrganization();
        $entity->name = self::organizationName;
        $entity->fullName = "Martin Pavelka";
        $entity->description = "Test case";
        $entity->storageLeaseSeconds = 0;
        $entity->deleteOnStorageLeaseExpiration = false;
        $entity->deploymentLeaseSeconds = 0;
        $entity->orgLdapMode = "NONE";
        $entity->canPublishCatalogs = false;
        $entity->isEnabled = true;
        Assert::type("VMware_VCloud_API_AdminOrgType", $this->app->createOrganizationFromEntity($entity));
    }*/

    public function testGetOrganizationRecordsByName() {

        $result = $this->app->getOrganizationRecordByName(self::organizationName);
        Assert::type("VMware_VCloud_API_AdminOrgType",$result);
        Assert::equal(self::organizationName, $result->get_name());
    }

    public function testGetOrganizationReferenceByName() {

        $result = $this->app->getOrganizationReferenceByName(self::organizationName);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal(self::organizationName, $result->get_name());
    }

    public function testGetNetworkByName() {

        $organization = $this->app->getOrganizationReferenceByName(self::organizationName);
        $result = $this->app->getNetworkRefByName($organization, self::networkName);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal(self::networkName, $result->get_name());
    }
}

// Run the tests
(new VOrgTest($container))->run();
