<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Models\VCloud\VProvider;
use Tester\Assert;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VProviderTest
 * @testCase
 */
class VProviderTest extends TestCase {

    /** @var VProvider $app */
    private $app;

    /** @var  VMware_VCloud_API_ReferenceType|null */
    private $provider;

    /** @var string  */
    private const providerName = "Brno DC4 Entry";

    /** @var string  */
    private const profileName = "Entry";

    /**
     * VProviderTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {

        $this->app = $container->getByType(VProvider::class);
        $this->provider = null;
    }

    public function testGetRoleRecordByName() {

       $result = $this->app->getProviderRecordByName(self::providerName);
       Assert::type("VMware_VCloud_API_ProviderVdcType",$result);
       Assert::equal($result->get_name(), self::providerName);
   }

    public function testGetRoleReferenceByName() {

        $this->provider = $this->app->getProviderReferenceByName(self::providerName);
        Assert::type("VMware_VCloud_API_ReferenceType",$this->provider);
        Assert::equal($this->provider->get_name(), self::providerName);
    }

    public function testGetStorageProfileReferenceByName() {

        $result = $this->app->getStorageProfileReferenceByName(self::profileName, $this->provider);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal($result->get_name(), self::profileName);
    }

}

// Run the tests
(new VProviderTest($container))->run();
