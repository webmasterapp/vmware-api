<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Models\VCloud\VRole;
use Tester\Assert;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VRoleTest
 * @testCase
 */
class VRoleTest extends TestCase {

    /** @var VRole $app */
    private $app;

    /** @var string  */
    private const roleName = "Organization Administrator";

    /**
     * VClientTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->app = $container->getByType(VRole::class);
    }

    public function testGetRoleRecordByName() {

       $result = $this->app->getRoleRecordByName(self::roleName);
       Assert::type("VMware_VCloud_API_RoleType",$result);
       Assert::equal(self::roleName, $result->get_name());
   }

    public function testGetRoleReferenceByName() {

        $result = $this->app->getRoleReferenceByName(self::roleName);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal(self::roleName, $result->get_name());
    }

}

// Run the tests
(new VRoleTest($container))->run();
