<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Models\VCloud\VTemplate;
use Tester\Assert;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VTemplateTest
 * @testCase
 */
class VTemplateTest extends TestCase {

    /** @var VTemplate $app */
    private $app;

    /** @var string  */
    private const templateName = "CentOS 7 minimal Brno";

    /**
     * VTemplateTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->app = $container->getByType(VTemplate::class);
    }

    public function testGetTemplateRecordByName() {

       $result = $this->app->getTemplateRecordByName(self::templateName);
       Assert::type("VMware_VCloud_API_QueryResultVAppTemplateRecordType",$result);
       Assert::equal(self::templateName, $result->get_name());
   }

    public function testGetTemplateReferenceByName() {

        $result = $this->app->getTemplateReferenceByName(self::templateName);
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
        Assert::equal(self::templateName, $result->get_name());
    }

    public function testGetVmByTemplate() {

        $result = $this->app->getVAppByTemplate($this->app->getTemplateReferenceByName(self::templateName));
        Assert::type("VMware_VCloud_API_ReferenceType",$result);
    }
}

// Run the tests
(new VTemplateTest($container))->run();
