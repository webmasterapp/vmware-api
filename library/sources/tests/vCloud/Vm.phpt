<?php /** @noinspection PhpUnhandledExceptionInspection */

use Library\Sources\Entities\VCloudVm;
use Library\Sources\Models\VCloud\VApp;
use Library\Sources\Models\VCloud\Vm;
use Library\Sources\Models\VCloud\VOrg;
use Library\Sources\Models\VCloud\VTemplate;
use Tester\TestCase;
use Nette\DI\Container;

$container = require __DIR__ . '/../../../configuration/bootstrap.php';

/**
 * Class VmTest
 * @testCase
 */
class VmTest extends TestCase {

    /** @var Vm */
    private $app;

    /** @var VApp */
    private $appVApp;

    /** @var VTemplate */
    private $appTemplate;

    /** @var VOrg */
    private $appOrganization;

    /** @var string */
    private const vAppName = "test6vApp";

    /** @var string  */
    private const templateName = "Ubuntu16LTS";

    /** @var string  */
    private const organizationName = "test6";

    /** @var string  */
    private const networkName = "internet";

    /** @var string  */
    private const vmName = "vmTest6";

    /**
     * VDCTest constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {

        $this->app = $container->getByType(Vm::class);
        $this->appVApp = $container->getByType(VApp::class);
        $this->appTemplate = $container->getByType(VTemplate::class);
        $this->appOrganization = $container->getByType(VOrg::class);
    }

    public function testCreateVmFromEntity() {

        $vm = new VCloudVm();
        $vm->vApp = $this->appVApp->getVAppReferenceByName(self::vAppName);
        $template = $this->appTemplate->getTemplateReferenceByName(self::templateName);
        $vm->sourceVApp = $this->appTemplate->getVAppByTemplate($template);
        $organization = $this->appOrganization->getOrganizationReferenceByName(self::organizationName);
        $vm->parentNetwork = $this->appOrganization->getNetworkRefByName($organization, self::networkName);
        $vm->name = self::vmName;
        $this->app->createVmFromEntity($vm);
    }
}

// Run the tests
(new VmTest($container))->run();
